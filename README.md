# React Carousel

## Installation

```sh
npm install @btpoe/react-carousel
```

or

```sh
yarn add @btpoe/react-carousel
```

## Documentation

Documentation can be found at https://btpoe.gitlab.io/react-carousel
