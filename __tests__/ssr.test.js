/**
 * @jest-environment node
 */

import React from 'react';
import ReactDOM from 'react-dom/server';
import Carousel from '../src';

describe('Server-side Rendering', () => {
  it('should not crash in node environments', () => {
    expect(() => {
      ReactDOM.renderToString(<Carousel />);
    }).not.toThrow();
  });

  it('should not crash in node environments with slides', () => {
    expect(() => {
      ReactDOM.renderToString(
        <Carousel>
          <div />
          <div />
          <div />
        </Carousel>
      );
    }).not.toThrow();
  });
});
