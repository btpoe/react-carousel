import React from 'react';
import { configure, addDecorator, addParameters } from '@storybook/react';
import { setDefaults } from '@storybook/addon-info';
import { create } from '@storybook/theming';
import './styles.css';

addParameters({
  options: {
    name: '@btpoe/react-carousel',
    theme: create({
      base: 'light',
      brandTitle: '@btpoe/react-carousel',
    }),
  },
});

setDefaults({
  inline: true,
});

addDecorator((story) => (
  <div style={{ maxWidth: '900px', margin: 'auto' }}>
    {story()}
  </div>
));

// automatically import all files ending in *.stories.js
const req = require.context('../stories', true, /\.stories\.js$/);
function loadStories() {
  require('../stories/getting-started.stories');
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
