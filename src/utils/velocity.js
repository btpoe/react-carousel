const TROTTLE_INTERVAL = 254;
const IDLE_INTERVAL = 1000;

const createVector = () => ({ position: null, time: null });

export default class Velocity {
  constructor() {
    this.reset();
  }

  reset() {
    this.tail = createVector();
    this.tween = createVector();
    this.head = createVector();
  }

  shouldSetTail() {
    // set if currently null
    if (!this.tail.time) return true;

    // set if enough time has passed for an accurate measurement
    if (this.head.time > this.tween.time + TROTTLE_INTERVAL) return true;

    // set if head changes directions
    const tweenDirection = this.tween.position > this.tail.position;
    const headDirection = this.head.position > this.tween.position;

    return tweenDirection !== headDirection;
  }

  updatePosition(position) {
    clearTimeout(this.idleTimeout);

    this.head = { position, time: Date.now() };

    if (this.shouldSetTail()) {
      this.tail = this.tween;
      this.tween = this.head;
    }

    this.idleTimeout = setTimeout(this.onIdle, IDLE_INTERVAL);
  }

  onIdle = () => {
    this.tween = this.head;
    this.tail = this.tween;
  };

  getVelocity() {
    if (!this.tail.time) return 0;

    const time = this.head.time - this.tail.time;

    if (time < 10) return 0;

    const distance = this.head.position - this.tail.position;

    return distance / time;
  }
}
