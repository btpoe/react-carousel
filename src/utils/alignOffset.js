const offset = (align, trackWidth, child) => {
  switch (align) {
    case 'left':
      return 0;
    case 'right':
      return trackWidth - child.clientWidth;
    default:
      return (trackWidth - child.clientWidth) / 2;
  }
};

export default offset;
