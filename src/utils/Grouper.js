import Group from '../Group';

export default class Grouper {
  constructor({ groupCells, ...config }) {
    this.config = config;
    this.groups = [];
    this.addGroup();

    switch (typeof groupCells) {
      case 'number': {
        this.type = 'count';
        this.value = groupCells;
      }
      break;
      case 'string': {
        this.type = 'width';
        this.value = groupCells.slice(0, -1) / 100;
      }
      break;
      case 'boolean': {
        if (groupCells) {
          this.type = 'width';
          this.value = 1;
        } else {
          this.type = 'count';
          this.value = 1;
        }
      }
      break;
      default: {
        this.type = 'count';
        this.value = 1;
      }
    }
  }

  get lastGroup() {
    return this.groups[this.groups.length - 1];
  }

  addGroup() {
    this.groups.push(new Group(this.config));
  }

  assign(slide) {
    switch (this.type) {
      case 'width': {
        if (this.lastGroup.clientWidth + slide.clientWidth > this.value * this.config.trackWidth) {
          this.addGroup();
        }
      }
      break;
      case 'count': {
        if (this.lastGroup.slides.length >= this.value) {
          this.addGroup();
        }
      }
      break;
    }

    this.lastGroup.add(slide);
  }
}
