import React from 'react';

const childrenEqual = (prevChildren, nextChildren) => {
  const prev = React.Children.toArray(prevChildren);
  const next = React.Children.toArray(nextChildren);

  if (prev.length !== next.length) {
    return false;
  }

  return prev.every((child, i) => (
    child.type === next[i].type && child.key === next[i].key
  ));
};

export default childrenEqual;
