import { useMemo } from 'react';

function evaluate(config, totalSlides) {
  if (Array.isArray(config)) {
    return config.every((config) => evaluate(config, totalSlides));
  }

  switch (typeof config) {
    case 'string':
      return matchMedia(config).matches;
    case 'number':
      return totalSlides >= config;
    case 'function':
      return !!config();
    default:
      return !!config;
  }
}

export default function useSetting(prop, totalSlides, ...triggers) {
  return useMemo(() => evaluate(prop, totalSlides), [prop, totalSlides, ...triggers]);
}
