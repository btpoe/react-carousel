import PropTypes from 'prop-types';

export default PropTypes.oneOfType([
  PropTypes.string,
  PropTypes.func,
  PropTypes.shape({ render: PropTypes.func.isRequired }),
]);
