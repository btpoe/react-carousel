import { useCallback, useMemo } from 'react';

export default ({ select, enabled, wrapAround, selectedIndex, totalSlides }) => {
  const previousSlide = useCallback((e) => {
    e.preventDefault();

    if (!enabled) return;

    select(selectedIndex - 1);
  }, [enabled, select, selectedIndex]);

  const nextSlide = useCallback((e) => {
    e.preventDefault();

    if (!enabled) return;

    select(selectedIndex + 1);
  }, [enabled, select, selectedIndex]);

  const dots = useMemo(() => {
    const arr = [];
    for (let i = 0; i < totalSlides; i++) {
      arr.push({
        active: i === selectedIndex,
        key: i + 1,
        onClick: (e) => {
          e.preventDefault();

          if (!enabled) return;

          select(i);
        },
      })
    }
    return arr;
  }, [enabled, selectedIndex, totalSlides]);

  const currentSlide = selectedIndex + 1;

  return {
    enabled,
    currentSlide,
    totalSlides,
    dots,
    hasNext: enabled && (wrapAround || currentSlide < totalSlides),
    hasPrevious: enabled && (wrapAround || selectedIndex > 0),
    nextSlide,
    previousSlide,
  };
};
