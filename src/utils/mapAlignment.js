export default (value) => {
  switch (value) {
    case 'top':
      return 'flex-start';
    case 'bottom':
      return 'flex-end';
    default:
      return value;
  }
};
