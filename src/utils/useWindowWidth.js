import { useState, useEffect } from 'react';

const isNodeEnv = typeof window === 'undefined';

export default function useWindowWidth() {
  const [innerWidth, setInnerWidth] = useState(isNodeEnv ? 320 : window.innerWidth);

  useEffect(() => {
    const onResize = () => {
      setInnerWidth(window.innerWidth);
    };

    window.addEventListener('resize', onResize);

    return () => {
      window.removeEventListener('resize', onResize);
    };
  }, []);

  return innerWidth;
}
