import React from 'react';

import components from './styleAdapters/plain';

export const defaultContext = {
  align: 'center',
  attraction: 1,
  components,
  enabled: true,
  friction: 1,
  onMove() {},
  onSettle() {},
  units: 'percent',
  verticalAlign: 'stretch',
  wrapAround: false,
  groupCells: false,
};

export default React.createContext(defaultContext);
