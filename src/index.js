import PropTypes from 'prop-types';
import React, { useState, useCallback, useEffect, useMemo, useRef } from 'react';

import Context from './Context';
import defaultComponents from './styleAdapters/plain';
import Track from './Track';
import PropTypesComponent from './utils/types/component';
import useRenderProps from './utils/useRenderProps';
import useWindowWidth from './utils/useWindowWidth';
import useSetting from './utils/useSetting';

const Carousel = React.forwardRef(({
  // misc settings
  enabled: enabledProp,
  isCarousel: isCarouselProp,
  wrapAround: wrapAroundProp,
  units,
  groupCells,

  // alignment
  align,
  verticalAlign,

  // physics
  attraction,
  friction,

  // events
  onMove,
  onSettle,

  // slides
  children: childrenProp,

  // controls
  controls,

  // custom styles
  components,

  // pass-through
  ...props
}, ref) => {
  const track = useRef();

  const [selectedIndex, setSelectedIndex] = useState(0);
  const [groups, setGroups] = useState([]);
  const innerWidth = useWindowWidth();

  const childRenderProps = {
    align,
    components,
    selectedIndex,
    verticalAlign,
  };

  const children = useMemo(() => {
    if (typeof childrenProp === 'function') {
      return childrenProp(childRenderProps)
    }
    return childrenProp;
  }, [childrenProp]);

  const totalSlides = useMemo(() => React.Children.count(children), [children]);

  const isCarousel = useSetting(isCarouselProp, totalSlides, innerWidth);
  const enabled = useSetting(enabledProp, totalSlides, innerWidth);
  const wrapAround = useSetting([wrapAroundProp, 2], totalSlides, innerWidth);

  const select = useCallback((newSelectedIndex, instant) => {
    if (!enabled) return;

    const lastIndex = track.current.groups.length - 1;

    if (newSelectedIndex < 0) {
      if (wrapAround) {
        newSelectedIndex = lastIndex;
      } else {
        newSelectedIndex = 0;
      }
    } else if (newSelectedIndex > lastIndex) {
      if (wrapAround) {
        newSelectedIndex = 0;
      } else {
        newSelectedIndex = lastIndex;
      }
    }

    setSelectedIndex(oldSelectedIndex => {
      if (oldSelectedIndex !== newSelectedIndex) {
        track.current.groups[oldSelectedIndex].selected = false;
      }

      return newSelectedIndex;
    });

    requestAnimationFrame(() => {
      track.current.groups[newSelectedIndex].selected = true;

      if (instant) {
        track.current.jump();
      } else {
        track.current.play();
      }
    });

  }, [enabled, wrapAround, track]);

  useEffect(() => {
    select(selectedIndex, true);
  }, []);

  const renderProps = useRenderProps({ select, enabled, wrapAround, selectedIndex, totalSlides: groups.length });

  const contextValue = {
    align,
    attraction,
    friction,
    onMove,
    onSettle,
    units,
    components,
    enabled,
    selectedIndex,
    verticalAlign,
    wrapAround,
    groupCells,
  };

  return isCarousel ? (
    <components.Container ref={ref} {...props}>
      <Context.Provider value={contextValue}>
        <Track select={select} ref={track} setGroups={setGroups}>
          {children}
        </Track>
        {controls(renderProps)}
      </Context.Provider>
    </components.Container>
  ) : (
    <React.Fragment>
      {children}
    </React.Fragment>
  );
});

Carousel.displayName = 'Carousel';

Carousel.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
  controls: PropTypes.func,
  // context
  align: PropTypes.oneOf(['left', 'center', 'right']),
  attraction: PropTypes.number,
  components: PropTypes.shape({
    Container: PropTypesComponent.isRequired,
    Slide: PropTypesComponent.isRequired,
    Track: PropTypesComponent.isRequired,
    View: PropTypesComponent.isRequired,
  }),
  enabled: PropTypes.any,
  friction: PropTypes.number,
  groupCells: PropTypes.any,
  isCarousel: PropTypes.any,
  onMove: PropTypes.func,
  onSettle: PropTypes.func,
  units: PropTypes.oneOf(['percent', 'pixel']),
  verticalAlign: PropTypes.oneOf(['stretch', 'flex-start', 'center', 'flex-end']),
  wrapAround: PropTypes.any,
};

Carousel.defaultProps = {
  children: null,
  controls: () => null,
  // context
  align: 'center',
  attraction: 1,
  components: defaultComponents,
  enabled: true,
  friction: 1,
  groupCells: false,
  isCarousel: true,
  onMove() {},
  onSettle() {},
  units: 'percent',
  verticalAlign: 'stretch',
  wrapAround: false,
};

export default Carousel;
