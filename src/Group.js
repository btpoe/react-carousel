import offset from './utils/alignOffset';

export default class Group {
  constructor(config) {
    this.config = config;
    this.slides = [];
    this.clientWidth = 0;
    this.targetX = 0;
  }

  add(slide) {
    this.slides.push(slide);

    this.clientWidth = slide.trueClient_Right - this.slides[0].clientLeft;
    this.targetX = this.slides[0].clientLeft - offset(this.config.align, this.config.trackWidth, this);
  }

  set selected(selected) {
    this._selected = selected;

    this.slides.forEach(slide => {
      slide.element.classList[selected ? 'add' : 'remove']('is-selected');
    });
  }

  get selected() {
    return this._selected;
  }
}
