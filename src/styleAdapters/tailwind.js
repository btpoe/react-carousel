import React from 'react';

const mergeClasses = (staticStyles, propClassName, otherProps) => {
  if (typeof staticStyles === 'function') {
    return `${propClassName} ${staticStyles(otherProps)}`;
  }

  return `${propClassName} ${staticStyles}`;
};

const mapAlignment = (value) => {
    switch (value) {
      case 'top':
        return 'items-start';
      case 'bottom':
        return 'items-end';
      case 'center':
        return 'items-center';
      case 'stretch':
        return 'items-stretch';
      default:
        return '';
    }
  };
  

const cleanProps = (props, without) => {
  const clone = { ...props };
  without.forEach((prop) => {
    delete clone[prop];
  });
  return clone;
};

const twind = (twClassNames, defaultComponent = 'div', without) =>
  React.forwardRef(
    ({ as: As = defaultComponent, className = '', ...props }, ref) => (
      <As
        className={mergeClasses(twClassNames, className, props)}
        ref={ref}
        {...(without ? cleanProps(props, without) : props)}
      />
    )
  );

const Container = twind('relative w-full');

const Slide = twind('shrink-0 relative');

const Track = twind(
  ({ verticalAlign }) => `flex w-full h-full will-change-transform ${mapAlignment(verticalAlign)}`,
  'div',
  ['verticalAlign']
);

const View = twind('h-full overflow-hidden touch-pan-y');

export default {
  Container,
  Slide,
  Track,
  View,
};
