import styled from 'styled-components';
import mapAlignment from '../utils/mapAlignment';

const Container = styled.div`
  position: relative;
  width: 100%;
`;

const Slide = styled.div`
  flex-shrink: 0;
  position: relative;
`;

const Track = styled.div`
  align-items: ${({ verticalAlign }) => mapAlignment(verticalAlign)};
  display: flex;
  height: 100%;
  width: 100%;
  will-change: transform;
`;

const View = styled.div`
  height: 100%;
  overflow: hidden;
  touch-action: pan-y;
`;

export default {
  Container,
  Slide,
  Track,
  View,
};
