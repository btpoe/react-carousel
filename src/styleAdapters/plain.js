import React from 'react';
import mapAlignment from '../utils/mapAlignment';

const mergeStyles = (staticStyles, propStyles, otherProps) => {
  if (typeof staticStyles === 'function') {
    return { ...staticStyles(otherProps), ...propStyles };
  }

  return { ...staticStyles, ...propStyles };
};

const cleanProps = (props, without) => {
  const clone = { ...props };
  without.forEach((prop) => {
    delete clone[prop];
  });
  return clone;
};

const styled = (styles, defaultComponent = 'div', without) =>
  React.forwardRef(
    ({ as: As = defaultComponent, style = {}, ...props }, ref) => (
      <As
        style={mergeStyles(styles, style, props)}
        ref={ref}
        {...(without ? cleanProps(props, without) : props)}
      />
    )
  );

const Container = styled({
  position: 'relative',
  width: '100%',
});

const Slide = styled({
  flexShrink: 0,
  position: 'relative',
});

const Track = styled(
  ({ verticalAlign }) => ({
    alignItems: mapAlignment(verticalAlign),
    display: 'flex',
    height: '100%',
    width: '100%',
    willChange: 'transform',
  }),
  'div',
  ['verticalAlign']
);

const View = styled({
  height: '100%',
  overflow: 'hidden',
  touchAction: 'pan-y',
});

export default {
  Container,
  Slide,
  Track,
  View,
};
