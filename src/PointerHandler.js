import PropTypes from 'prop-types';
import React from 'react';

import CarouselContext from './Context';
import Velocity from './utils/velocity';

const handlePointerEnd = (type, fn) => {
  window[`${type}EventListener`]('mouseup', fn);
  window[`${type}EventListener`]('mouseleave', fn);
  window[`${type}EventListener`]('touchend', fn);
  window[`${type}EventListener`]('touchcancel', fn);
};

const handlePointerMove = (type, fn) => {
  window[`${type}EventListener`]('mousemove', fn);
  window[`${type}EventListener`]('touchmove', fn);
};

const normalizeTouch = (e) => {
  if (e.touches && e.touches.length) {
    e.clientX = e.touches[0].clientX;
    e.clientY = e.touches[0].clientY;
  }
};

export default class PointerHandler extends React.PureComponent {
  static displayName = 'PointerHandler';

  static contextType = CarouselContext;

  static propTypes = {
    adjustX: PropTypes.func.isRequired,
    applyForce: PropTypes.func.isRequired,
    children: PropTypes.node.isRequired,
  };

  isDragging = false;

  v = new Velocity();

  componentWillUnmount() {
    handlePointerEnd('remove', this.onMove);
    handlePointerEnd('remove', this.onEnd);
  }

  onStart = (e) => {
    normalizeTouch(e);

    this.isDragging = true;
    this.v.reset();
    this.initialX = e.clientX;
    this.initialY = e.clientY;
    this.lastX = this.initialX;
    this.scrollLocked = false;
    handlePointerMove('add', this.onMove);
    handlePointerEnd('add', this.onEnd);
  };

  onMouseStart = (e) => {
    if (e.altKey) return;
    if (e.ctrlKey) return;
    if (e.metaKey) return;
    if (e.button !== 0) return;

    this.onStart(e);
  };

  onMove = (e) => {
    if (!this.isDragging) return;

    normalizeTouch(e);

    const clientX = e.clientX;

    const clientXDelta = Math.abs(this.initialX - clientX);
    const clientYDelta = Math.abs(this.initialY - e.clientY);

    if (!this.scrollLocked && clientXDelta > 10) {
      this.scrollLocked = true;
      window.addEventListener('click', this.preventClick);
    }

    if (!this.scrollLocked && clientYDelta > 10 && clientYDelta > clientXDelta) {
      this.v.reset();
      this.onEnd(e);
      return;
    }

    if (e.type === 'mousemove') {
      e.preventDefault();
    }

    this.v.updatePosition(clientX);
    this.props.adjustX(clientX - this.lastX);
    this.lastX = clientX;
  };

  onEnd = () => {
    this.isDragging = false;
    this.props.applyForce(this.v.getVelocity());
    handlePointerEnd('remove', this.onMove);
    handlePointerEnd('remove', this.onEnd);
  };

  preventClick = (e) => {
    e.preventDefault();
    window.removeEventListener('click', this.preventClick);
  }

  render() {
    const { components, enabled } = this.context;
    const { children } = this.props;

    return (
      <components.View
        onMouseDown={enabled ? this.onMouseStart : undefined}
        onTouchStart={enabled ? this.onStart : undefined}
      >
        {children}
      </components.View>
    );
  }
}
