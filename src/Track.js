import PropTypes from 'prop-types';
import React from 'react';

import CarouselContext from './Context';
import PointerHandler from './PointerHandler';
import childrenEqual from './utils/childrenEqual';
import Grouper from './utils/Grouper';

const NORMALIZE_FRICTION = 0.25;
const NORMALIZE_ATTRACTION = 0.04;
const NORMALIZE_FORCE = 16;

export default class Track extends React.Component {
  static displayName = 'Track';

  static contextType = CarouselContext;

  static propTypes = {
    children: PropTypes.node,
    select: PropTypes.func.isRequired,
  };

  _x = 0;

  node = React.createRef();

  pointer = React.createRef();

  velocity = 0;

  componentDidMount() {
    window.addEventListener('resize', this.recalculate);
    this.cacheContext();
    this.recalculate();
  }

  componentDidUpdate(prevProps) {
    // if settings have changed, recalculate caches
    if (
      this._align !== this.context.align ||
      this._wrapAround !== this.context.wrapAround ||
      this._groupCells !== this.context.groupCells
    ) {
      this.recalculate();
    }
    // if children changed, recalculate caches without updating track position
    else if (!childrenEqual(prevProps.children, this.props.children)) {
      this.recalculate({ silent: true });
    }

    // unset shifted slides if wrapAround has been disabled since last update
    if (this._wrapAround && !this.context.wrapAround) {
      this.firstSlide.element.style.left = '';
      this.lastSlide.element.style.left = '';
    }

    this.cacheContext();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.recalculate);
    cancelAnimationFrame(this.frameQueued);
    clearTimeout(this.playTimer);
  }

  cacheContext() {
    this._align = this.context.align;
    this._wrapAround = this.context.wrapAround;
    this._groupCells = this.context.groupCells;
  }

  get restingPosition() {
    return this.x + this.velocity / (1 - this.friction);
  }

  get friction() {
    return 1 - this.context.friction * NORMALIZE_FRICTION;
  }

  get currentGroup() {
    return this.groups[this.context.selectedIndex] || {};
  }

  get firstSlide() {
    return this.slides[0] || {};
  }

  get lastSlide() {
    return this.slides[this.slides.length - 1] || {};
  }

  get firstGroup() {
    return this.groups[0] || {};
  }

  get lastGroup() {
    return this.groups[this.groups.length - 1] || {};
  }

  get pastLeftEdge() {
    return -this.x < this.lastSlide.wrapAroundTrigger;
  }

  get pastRightEdge() {
    return -this.x > this.firstSlide.wrapAroundTrigger;
  }

  get progress() {
    return -this.x / this.lastGroup.targetX;
  }

  get x() {
    return this._x;
  }

  set x(x) {
    if (this.context.wrapAround) {
      // shift the track if necessary to simulate wrap around
      if (
        -x >
        this.lastSlide.clientRight +
          this.firstSlide.clientWidth -
          this.clientWidth
      ) {
        x += this.lastSlide.clientRight;
      } else if (x > this.lastSlide.clientWidth) {
        x -= this.lastSlide.clientRight;
      }
    }

    this._x = x;

    // only render x once per frame
    if (!this.frameQueued) {
      this.frameQueued = requestAnimationFrame(() => {
        this.frameQueued = null;

        // using percentages so that on resize, everything stays in proportion
        this.domTrack.style.transform = `translateX(${this.toUnits(this._x)})`;

        if (this.context.wrapAround) {
          // shift first and last slide if track is beyond either edge
          if (this.pastRightEdge) {
            this.firstSlide.element.style.left = this.toUnits(
              this.lastSlide.clientRight
            );
          } else {
            this.firstSlide.element.style.left = '';
          }
          if (this.pastLeftEdge) {
            this.lastSlide.element.style.left = this.toUnits(
              -this.lastSlide.clientRight
            );
          } else {
            this.lastSlide.element.style.left = '';
          }
        }

        this.context.onMove({
          progress: this.progress,
          selectedIndex: this.context.selectedIndex,
        });
      });
    }
  }

  get domTrack() {
    return this.node.current;
  }

  recalculate = ({ silent } = {}) => {
    // cache all the dom measurements
    const { align } = this.context;

    this.clientWidth = this.domTrack.clientWidth;

    if (this.slides && this.context.wrapAround) {
      // remove wrap around offsets for proper calculations
      this.firstSlide.element.style.left = '';
      this.lastSlide.element.style.left = '';
    }

    this.slides = [...this.domTrack.children].map((element) => ({
      element,
      clientWidth: element.clientWidth,
      clientLeft: element.offsetLeft,
      trueClient_Right: element.offsetLeft + element.clientWidth,
      clientRight:
        element.offsetLeft +
        element.clientWidth +
        parseInt(
          window.getComputedStyle(element).getPropertyValue('margin-right'),
          10
        ),
    }));

    const grouper = new Grouper({
      ...this.context,
      trackWidth: this.clientWidth,
    });
    this.slides.forEach((slide, i) => grouper.assign(slide));
    this.groups = grouper.groups;

    this.props.setGroups(this.groups);

    if (this.context.wrapAround) {
      this.firstSlide.wrapAroundTrigger =
        this.lastSlide.clientRight -
        this.clientWidth -
        this.lastSlide.clientWidth / 2;
      this.lastSlide.wrapAroundTrigger =
        this.firstSlide.clientRight - this.firstSlide.clientWidth / 2;
      this.firstGroup.wrapAroundX =
        this.firstGroup.targetX + this.lastSlide.clientRight;
      this.lastGroup.wrapAroundX =
        this.lastGroup.targetX - this.lastSlide.clientRight;
    }

    // if not silent, update x with new computed values
    if (!silent) {
      this.x = -this.currentGroup.targetX;
    }
  };

  applyForce = (force) => {
    cancelAnimationFrame(this.queueFrame);

    this.velocity = force * NORMALIZE_FORCE;

    const restingX = this.restingPosition;
    const { selectedIndex: currentIndex } = this.context;

    let newIndex = this.groups.reduce(
      (closest, current, index) => {
        const delta = Math.min(
          Math.abs(-restingX - current.targetX),
          this.groupCanWrap(current)
            ? Math.abs(-restingX - current.wrapAroundX)
            : Infinity
        );

        if (delta < closest.delta) {
          return { delta, index };
        }
        return closest;
      },
      { delta: Infinity }
    ).index;

    // selected slide didn't change
    if (currentIndex === newIndex) {
      if (force > 0.3) {
        newIndex--;
      } else if (force < -0.3) {
        newIndex++;
      }
    }

    this.props.select(newIndex);
  };

  adjustX = (delta) => {
    this.x += delta;
  };

  play = () => {
    clearTimeout(this.playTimer);
    // override animation if pointer is active
    if (this.pointer.current.isDragging) return;

    // apply physics
    this.applyAttraction();
    this.applyFriction();

    // if track is still moving, queue up the next frame
    if (Math.abs(this.velocity) > 0.005) {
      // physics needs to be independent from render preformance
      this.playTimer = setTimeout(this.play, 16);
    } else {
      this.jump();
    }
  };

  jump = () => {
    this.x = -this.currentGroup.targetX;
    this.context.onSettle({
      progress: this.progress,
      selectedIndex: this.context.selectedIndex,
    });
  };

  applyAttraction() {
    // if wrapAround is enabled, check if the ends are closer
    let wrapAroundCloser = false;

    // only look test for wrap around on the slides on either end.
    if (this.groupCanWrap(this.currentGroup)) {
      wrapAroundCloser =
        Math.abs(-this.currentGroup.wrapAroundX - this.x) <
        Math.abs(-this.currentGroup.targetX - this.x);
    }

    const distance = wrapAroundCloser
      ? -this.currentGroup.wrapAroundX - this.x
      : -this.currentGroup.targetX - this.x;

    const force = distance * (this.context.attraction * NORMALIZE_ATTRACTION);

    this.velocity += force;
  }

  applyFriction() {
    this.x += this.velocity;
    this.velocity *= this.friction;
  }

  toUnits(value) {
    switch (this.context.units) {
      case 'percent':
        return `${(value * 100) / this.clientWidth}%`;
      default:
        return `${value}px`;
    }
  }

  groupCanWrap(group) {
    return (
      this.context.wrapAround &&
      (group === this.firstGroup || group === this.lastGroup)
    );
  }

  render() {
    const { components, verticalAlign } = this.context;
    const { children } = this.props;

    return (
      <PointerHandler
        ref={this.pointer}
        adjustX={this.adjustX}
        applyForce={this.applyForce}
      >
        <components.Track
          verticalAlign={verticalAlign}
          ref={this.node}
          onLoad={this.recalculate}
        >
          {children}
        </components.Track>
      </PointerHandler>
    );
  }
}
