import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

import Carousel from '../src';

storiesOf('Getting Started', module)
  .addDecorator(withInfo)
  .add(
    'Installation',
    () => (
      <Carousel />
    ),
    {
      info: `
        \`
        npm install @btpoe/react-carousel
        \`

        or

        \`
        yarn add @btpoe/react-carousel
        \`
      `,
    },
  );
