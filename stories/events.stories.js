import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { boolean, number, text, withKnobs } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import Carousel from '../src';
import { makeSlides } from './helpers';

const butonStyles = {
  appearance: 'none',
  background: '#eee',
  border: '0',
  borderRadius: '6px',
  padding: '5px',
};

const dotStyles = {
  ...butonStyles,
};

storiesOf('Events', module)
  .addDecorator(withKnobs)
  .add(
    'onMove',
    () => (
      <Carousel onMove={action('onMove')} wrapAround={boolean('Wrap Around', false)}>{makeSlides}</Carousel>
    ),
    {
      info: `
        This event is called once per frame while the carousel position is being updated.
        The following keys are provided to the first argument of the callback:

        <pre>
        {
          progress,
        }
        </pre>
      `,
    },
  )
  .add(
    'onSettle',
    () => (
      <Carousel onSettle={action('onSettle')} wrapAround={boolean('Wrap Around', false)}>{makeSlides}</Carousel>
    ),
    {
      info: `
        This event is called once when the carousel stops moving.
        The following keys are provided to the first argument of the callback:

        <pre>
        {
          selectedIndex,
        }
        </pre>
      `,
    },
  );
