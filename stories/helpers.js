import React from 'react';

export const makeSlides = ({ components: { Slide } }) => [
  <Slide key="1" style={{ width: '100%' }}>
    <img src="https://picsum.photos/id/1018/900/600" />
  </Slide>,
  <Slide key="2" style={{ width: '100%' }}>
    <img src="https://picsum.photos/id/1019/900/600" />
  </Slide>,
  <Slide key="3" style={{ width: '100%' }}>
    <img src="https://picsum.photos/id/1000/900/600" />
  </Slide>,
  <Slide key="4" style={{ width: '100%' }}>
    <img src="https://picsum.photos/id/1002/900/600" />
  </Slide>,
];

export const makeUnevenSlides = ({ components: { Slide } }) => [
  <Slide key="1" style={{ width: '80%' }}>
    <img src="https://picsum.photos/id/1018/720/580" />
  </Slide>,
  <Slide key="2" style={{ width: '80%' }}>
    <img src="https://picsum.photos/id/1019/720/500" />
  </Slide>,
  <Slide key="3" style={{ width: '80%' }}>
    <img src="https://picsum.photos/id/1000/720/600" />
  </Slide>,
  <Slide key="4" style={{ width: '80%' }}>
    <img src="https://picsum.photos/id/1002/720/400" />
  </Slide>,
];

export const makeSmallSlides = ({ components: { Slide } }) => [
  <Slide key="1" style={{ margin: '0 15px' }}>
    <img src="https://picsum.photos/id/1018/280/240" />
  </Slide>,
  <Slide key="2" style={{ margin: '0 15px' }}>
    <img src="https://picsum.photos/id/1019/200/240" />
  </Slide>,
  <Slide key="3" style={{ margin: '0 15px' }}>
    <img src="https://picsum.photos/id/1000/400/240" />
  </Slide>,
  <Slide key="4" style={{ margin: '0 15px' }}>
    <img src="https://picsum.photos/id/1002/100/240" />
  </Slide>,
  <Slide key="5" style={{ margin: '0 15px' }}>
    <img src="https://picsum.photos/id/1018/180/240" />
  </Slide>,
  <Slide key="6" style={{ margin: '0 15px' }}>
    <img src="https://picsum.photos/id/1019/250/240" />
  </Slide>,
  <Slide key="7" style={{ margin: '0 15px' }}>
    <img src="https://picsum.photos/id/1000/750/240" />
  </Slide>,
  <Slide key="8" style={{ margin: '0 15px' }}>
    <img src="https://picsum.photos/id/1002/300/240" />
  </Slide>,
];
