import React from 'react';
import { storiesOf } from '@storybook/react';
import { boolean, number, text, withKnobs } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import Carousel from '../src';
import { makeSlides } from './helpers';

const slideStyles = {
  color: 'white',
  display: 'flex',
  flexDirection: 'column',
  flexShrink: '0',
  padding: '20px',
  width: '100%',
  minHeight: '500px',
};

const alignRight = {
  alignItems: 'flex-end',
  textAlign: 'right',
};

storiesOf('Slides', module)
  .addDecorator(withKnobs)
  .add('as plain children', withInfo(`
    Use like a regular children.
  `)(() => (
    <Carousel>
      <div style={{ ...slideStyles, background: '#60b11c' }}>
        <h2>Lorem Ipsum</h2>
        <p style={{ width: '80%' }}>
          Dolor sit amet, consectetur adipiscing elit. In sodales at lectus sit amet dictum.
          Aliquam vehicula eros vel turpis euismod, feugiat tristique nunc posuere. Curabitur faucibus
          felis id dignissim varius. Nunc vitae commodo nulla. Mauris diam augue, sagittis sit amet sagittis nec,
          molestie in nunc. Vivamus ut finibus tellus. Integer ut tincidunt est. Sed tincidunt nunc in maximus
          tincidunt.
        </p>
      </div>
      <div style={{ ...slideStyles, ...alignRight, justifyContent: 'flex-end', background: '#ca0909' }}>
        <h2>Lorem Ipsum</h2>
        <p style={{ width: '80%' }}>
          Dolor sit amet, consectetur adipiscing elit. In sodales at lectus sit amet dictum.
          Aliquam vehicula eros vel turpis euismod, feugiat tristique nunc posuere. Curabitur faucibus
          felis id dignissim varius. Nunc vitae commodo nulla. Mauris diam augue, sagittis sit amet sagittis nec,
          molestie in nunc. Vivamus ut finibus tellus. Integer ut tincidunt est. Sed tincidunt nunc in maximus
          tincidunt.
        </p>
      </div>
      <div style={{ ...slideStyles, justifyContent: 'flex-end', background: '#c740c0' }}>
        <h2>Lorem Ipsum</h2>
        <p style={{ width: '80%' }}>
          Dolor sit amet, consectetur adipiscing elit. In sodales at lectus sit amet dictum.
          Aliquam vehicula eros vel turpis euismod, feugiat tristique nunc posuere. Curabitur faucibus
          felis id dignissim varius. Nunc vitae commodo nulla. Mauris diam augue, sagittis sit amet sagittis nec,
          molestie in nunc. Vivamus ut finibus tellus. Integer ut tincidunt est. Sed tincidunt nunc in maximus
          tincidunt.
        </p>
      </div>
    </Carousel>
  )))
  .add('as render prop', withInfo(`
    If a function is provided as the children of a carousel, it will execute the function with the following settings
    provided as a first argument:

    <pre>
    {
      align,
      components: { Slide },
      selectedIndex,
      verticalAlign,
    }
    </pre>

    It is important to return an array of slides not wrapped in <React.Fragment />. Otherwise, it will evaluate as 1 child
    and break certain logic.
  `)(() => (
    <Carousel>{makeSlides}</Carousel>
  )));
