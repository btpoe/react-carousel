import React from 'react';
import { storiesOf } from '@storybook/react';
import { boolean, number, text, withKnobs } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import Carousel from '../src';
import { makeSlides } from './helpers';

storiesOf('Controls', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add(
    'buttons',
    () => (
      <Carousel
        controls={({ nextSlide, previousSlide }) => (
          <>
            <button className="prev" onClick={previousSlide}>previous</button>
            <button className="next" onClick={nextSlide}>next</button>
          </>
        )}
      >
        {makeSlides}
      </Carousel>
    ),
    {
      info: `
        The controls prop is a render function with the following keys provided to the first argument:

        <pre>
        {
          enabled,
          currentSlide,
          totalSlides,
          dots, // see pagination
          hasNext, // boolean that will be \`false\` when the last slide is selected
          hasPrevious, // boolean that will be \`false\` when the first slide is selected
          nextSlide, // callback function to go to the next slide
          previousSlide, // callback function to go to the previous slide
        }
        </pre>
      `,
    },
  )
  .add(
    'pagination',
    () => (
      <Carousel
        controls={({ dots }) => (
          <div className="dots">
            {dots.map(({ active, ...dotProps }) => (
              <button className={`dot ${active ? 'active' : ''}`} {...dotProps}>{dotProps.key}</button>
            ))}
          </div>
        )}
      >
        {makeSlides}
      </Carousel>
    ),
    {
      info: `
        The controls prop is a render function with the following keys provided to the first argument:

        <pre>
        {
          enabled,
          currentSlide,
          totalSlides,
          dots,
          hasNext,
          hasPrevious,
          nextSlide,
          previousSlide,
        }
        </pre>

        \`dots\` is an array matching the number of slides. Each array item is an object with the following properties:

        <pre>
        {
          active, // boolean of if the selected slide index matches this dot's index
          key, // nth item in the array
          onClick, // callback select the slide of the corresponding index
        }
        </pre>
      `
    },
  );
