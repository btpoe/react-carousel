import React from 'react';
import { storiesOf } from '@storybook/react';
import { boolean, number, text, select, withKnobs } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import Carousel from '../src';
import { makeSlides, makeUnevenSlides, makeSmallSlides } from './helpers';

storiesOf('Settings', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add(
    'enabled',
    () => (
      <Carousel
        enabled={[
          boolean('enabled (Boolean)', true),
          number('enabled (Number - `true` if children.length >= n)', 3),
          text('enabled (String - returns matchMedia result)', '(min-width: 10px)'),
        ]}
      >
        {makeSlides}
      </Carousel>
    ),
    {
      info: `
        Turning this setting off will result in the carousel freezing in it's current state.
        Dragging, previous and next buttons, and page dots will all take no action if interacted with.

        Prop can take any type. If it is:

        - Boolean: passed through as-is.
        - Function: will execuate and coerce a boolean value from result.
        - Number: will evaluate as \`true\` if the number of children is greater or equal to the prop value.
        - String: treated as a media query to be passed into \`matchMedia\`.
        - Array: will iterate through the items using the above rules. Will only be \`true\` if every rule is true.

        A boolean will be coerced from a value of any other type provided, such as an object or null.
      `,
    },
  )
  .add(
    'isCarousel',
    () => (
      <Carousel
        isCarousel={[
          boolean('isCarousel (Boolean)', true),
          number('isCarousel (Number - `true` if children.length >= n)', 3),
          text('isCarousel (String - returns matchMedia result)', '(min-width: 10px)'),
        ]}
      >
        {makeSlides}
      </Carousel>
    ),
    {
      info: `
        Turning this setting off will result in the carousel removing all wrapping elements that construct
        the carousel and passthrough the children in their original state.

        Prop can take any type. If it is:

        - Boolean: passed through as-is.
        - Function: will execuate and coerce a boolean value from result.
        - Number: will evaluate as \`true\` if the number of children is greater or equal to the prop value.
        - String: treated as a media query to be passed into \`matchMedia\`.
        - Array: will iterate through the items using the above rules. Will only be \`true\` if every rule is true.

        A boolean will be coerced from a value of any other type provided, such as an object or null.
      `,
    },
  )
  .add(
    'wrapAround',
    () => (
      <Carousel
        wrapAround={[
          boolean('wrapAround (Boolean)', true),
          number('wrapAround (Number - `true` if children.length >= n)', 3),
          text('wrapAround (String - returns matchMedia result)', '(min-width: 10px)'),
        ]}
      >
        {makeSlides}
      </Carousel>
    ),
    {
      info: `
        Turning this setting off will result in the carousel stopping at each end of the track.

        Prop can take any type. If it is:

        - Boolean: passed through as-is.
        - Function: will execuate and coerce a boolean value from result.
        - Number: will evaluate as \`true\` if the number of children is greater or equal to the prop value.
        - String: treated as a media query to be passed into \`matchMedia\`.
        - Array: will iterate through the items using the above rules. Will only be \`true\` if every rule is true.

        A boolean will be coerced from a value of any other type provided, such as an object or null.
      `,
    },
  )
  .add(
    'alignment',
    () => (
      <Carousel
        align={select('Align', { center: 'center', left: 'left', right: 'right' }, 'center')}
        verticalAlign={select('Vertical Align', { stretch: 'stretch', center: 'center', 'top': 'top', 'bottom': 'bottom' }, 'stretch')}
        wrapAround={boolean('Wrap Around', false)}
      >
        {makeUnevenSlides}
      </Carousel>
    ),
    {
      info: `
        Align: Horizontal anchor the slides will settle to relative to the carousel viewport

        Vertical Align: Vertical alignment of the slides relative to each other
      `,
    },
  )
  .add(
    'physics',
    () => (
      <Carousel
        attraction={number('Attraction', 1)}
        friction={number('Friction', 1)}
        wrapAround={boolean('Wrap Around', false)}
      >
        {makeSlides}

      </Carousel>
    ),
    {
      info: `
        Attraction: how much magnetic force there is to move the track towards the selected slide.

        Friction: how much resistance there is to move the track towards the selected slide.
      `,
    },
  )
  .add(
    'groupCells (boolean)',
    () => (
      <Carousel groupCells={boolean('Group Cells', true)}>
        {makeSmallSlides}
      </Carousel>
    ),
    {
      info: `
        This prop will allows for multiple cells to be selected at the same time

        Prop can take any type. If it is:

        - Boolean: \`false\` is the same as passing \`1\`, \`true\` is the same as passing \`'100%'\`
        - Number: will evaluate as \`true\` if the number of children is greater or equal to the prop value.
        - String: a percentage width.
      `,
    },
  )
  .add(
    'groupCells (number)',
    () => (
      <Carousel
        groupCells={number('Group Cells', 3)}
        controls={({ nextSlide, previousSlide, dots }) => (
          <>
            <button className="prev" onClick={previousSlide}>previous</button>
            <button className="next" onClick={nextSlide}>next</button>
            <div>
              {dots.map((props) => (
                <button {...props}></button>
              ))}
            </div>
          </>
        )}
      >
        {makeSmallSlides}
      </Carousel>
    ),
    {
      info: `
        This prop will allows for multiple cells to be selected at the same time

        Prop can take any type. If it is:

        - Boolean: \`false\` is the same as passing \`1\`, \`true\` is the same as passing \`'100%'\`
        - Number: will evaluate as \`true\` if the number of children is greater or equal to the prop value.
        - String: a percentage width.
      `,
    },
  )
  .add(
    'groupCells (string)',
    () => (
      <Carousel groupCells={text('Group Cells', '80%')}>
        {makeSmallSlides}
      </Carousel>
    ),
    {
      info: `
        This prop will allows for multiple cells to be selected at the same time

        Prop can take any type. If it is:

        - Boolean: \`false\` is the same as passing \`1\`, \`true\` is the same as passing \`'100%'\`
        - Number: will evaluate as \`true\` if the number of children is greater or equal to the prop value.
        - String: a percentage width.
      `,
    },
  );